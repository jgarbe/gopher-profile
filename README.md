![UMN Gopher](images/goldy.png) Gopher-profile
=======================

Easy-to-use tools for profiling cpu and memory usage on an individual computer or multi-node PBS jobs

INSTALLATION
------------

Gopher-profile is distributed as a collection of Perl scripts compatible with Linux and MacOS. Download the latest release of the sourcecode and uncompress it. Copy the contents of the bin folder to a folder in your path.

**Dependencies**

- multi-profile.pl: R and the R library ggplot2

USAGE
-----

Use the "-h" option to get usage information for any Gopher-profile script. 

ISSUES
------

Use the associated github issue track to report issues: https://github.umn.edu/jgarbe/gopher-profile/issues

AKNOWLEDGEMENTS
---------------

Gopher-profile is developed by the Research Informatics Solutions (RIS) group at the [University of Minnesota Supercomputing Institute](http://www.msi.umn.edu). It is released under the terms of the GNU General Public License.

Copyright (c) 2013-2020 John Garbe 

![UMN Logo](images/umnlogo.gif)